package main

import (
	"fmt"
	"net/http"
)

var GitBranch string

func main() {
	http.HandleFunc("/", HelloServer)
	http.ListenAndServe(":8080", nil)
}

func HelloServer(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello world, from branch: %s\n", GitBranch)
}
